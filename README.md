## Небольшое описание

Дипломная практическая работа выполнена на основе bare-metal kubernetes c containerd-runtime.

Используются 3 виртуальные машины (мастер-нода и 2 рабочие ноды), созданные через terraform с сохранением состояния в S3-яндекс.

Внешний балансировщик ведет на nginx, который проксирует на nodeport-сервисы приложения (frontend/backend).

Привязан домен prom.profywww.ru c сертификатом letsencrypt и прописана днс-зона в console.yandex

(днс-серверы яндекса отмечены в хост-провайдере домена).

В свою очередь этот домен указан в обработчике балансировщика.

Также настроены сервисы prometheus, grafana и loki.

Использовался только один gitlab-репозиторий.

Kubernetes описан как отдельными манифестами, так и в составе helm-чартов.

Приведен полный CI/CD:
- сборка докер-образов и публикация в gitlab-registry
- сканирование на ошибки кода
- упаковка в helm-пакеты
- публикация образов в nexus
- ротация helm-пакетов во время деплоя на сервере с nexus helm-hosting репозитория
Пельменная доступна по адресу https://diplom.profywww.ru

## Развертывание приложения

для развертывания приложения достаточно запустить пайплайн на страничке
https://gitlab.praktikum-services.ru/std-024-48/diplom_momo_app/-/pipelines

## Чеклист

### Код хранится в GitLab с использованием любого git-flow
Один репозиторий с одной веткой master c энвами, регистри и 2я пайплайнами:
https://gitlab.praktikum-services.ru/std-024-48/sausage-store2
### В проекте присутствует .gitlab-ci.yml, в котором описаны шаги сборки
В репозитории общий пайплайн в корне, 
который включает в себя два других из папок
frontend и backend
![Alt text](image-1.png)
### Артефакты сборки (бинарные файлы, docker-образы или др.) публикуются в систему хранения (Nexus или аналоги)
Все необходимые для kubernetes helm-чарты хранятся в репозитории nexus:
https://nexus.praktikum-services.tech/repository/momo-store-helm-maksim-koldaev-24
### Артефакты сборки версионируются
версионируются чарты приложений пельменной: frontend, backend
![Alt text](image-3.png)
### Написаны Dockerfile'ы для сборки Docker-образов бэкенда и фронтенда
- Бэкенд: бинарный файл Go в Docker-образе
- Фронтенд: HTML-страница раздаётся с Nginx

![Alt text](image-4.png)
![Alt text](image-5.png)
![Alt text](image-6.png)
### В GitLab CI описан шаг сборки и публикации артефактов
### В GitLab CI описан шаг тестирования
### В GitLab CI описан шаг деплоя
Все вышеуказанные шаги соблюдены в пайплайнах:
https://gitlab.praktikum-services.ru/std-024-48/diplom_momo_app/-/blob/main/backend/.gitlab-ci.yml
https://gitlab.praktikum-services.ru/std-024-48/diplom_momo_app/-/blob/main/frontend/.gitlab-ci.yml
### Развёрнут Kubernetes-кластер в облаке
![Alt text](image-7.png)
![Alt text](image-8.png)
![Alt text](image-9.png)
### Kubernetes-кластер описан в виде кода, и код хранится в репозитории GitLab
В папке helm написаны два чарта backend и frontend:
https://gitlab.praktikum-services.ru/std-024-48/diplom_momo_app/-/tree/main/helm
### Конфигурация всех необходимых ресурсов описана согласно IaC
В папке terraform описана вся необходимая конфигурация, согласно IaC; именно через terraform были созданы три виртуальные машины для k8s:
![Alt text](image-10.png)
### Картинки, которые использует сайт, или другие небинарные файлы, необходимые для работы, хранятся в S3
все верно - там и хранятся как на рисунки выше +
в консоли браузера видно, что картинки для пельменной подтягиваются именно с бакета:
![Alt text](image-11.png)
### Секреты не хранятся в открытом виде
он хранится только на сервере:
![Alt text](image-12.png)
### Написаны Kubernetes-манифесты для публикации приложения
есть в папке kubernetes отдельные манифесты:
![Alt text](image-13.png)
### Написан Helm-чарт для публикации приложения
есть в папке helm c использованием values, в которых автоматически переписывается версия чарта для версионности в пайплайне:
![Alt text](image-14.png)
![Alt text](image-15.png)
### Helm-чарты публикуются и версионируются в Nexus
вот скрины из пайплайна и репозитория https://nexus.praktikum-services.tech/repository/momo-store-helm-maksim-koldaev-24
![Alt text](image-16.png)
![Alt text](image-17.png)
### Приложение подключено к системам логирования и мониторинга
Я связал специальный location "metrics" с prometheus
https://diplom.profywww.ru/metrics
а также настроил и опубликовал чарты prometheus, loki и grafana в nexus
https://nexus.praktikum-services.tech/repository/momo-store-helm-maksim-koldaev-24
Все 4 чарта мониторинга опубликованы через NodePort-сервисы и проксируются в nginx (папка nginx):
(обязательно есть группа безопасности)
![Alt text](image-23.png)
![Alt text](image-22.png)
![Alt text](image-18.png)
![Alt text](image-19.png)
![Alt text](image-20.png)
![Alt text](image-21.png)
![Alt text](image-24.png)
### Есть дашборд, в котором можно посмотреть логи и состояние приложения
- дашбоард с кастомными метриками
| requests_count{instance="backend:8081", job="kube-state-metrics"} |
| response_timing_ms_bucket{handler="/categories/", instance="backend:8081", job="kube-state-metrics",le="100.0"}
 |
| 
response_timing_ms_count{handler="/products/", instance="backend:8081", job="kube-state-metrics"}
 |
| kube_apiserver_pod_logs_backend_tls_failure_total |
| go_gc_heap_allocs_by_size_bytes_count |
- дашбоард с логами приложений
- дашбоард с визуализацией kubernetes
![Alt text](image-25.png)
![Alt text](image-26.png)
![Alt text](image-27.png)