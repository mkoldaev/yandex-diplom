#!/bin/bash
cd /home/ubuntu/git/diplom_momo_app;
kubectl apply -f ./kubernetes/docker_secret.yaml || true;
kubectl apply -f ./kubernetes/backend/ && wait;
kubectl apply -f ./kubernetes/frontend/ && wait;
helm upgrade --atomic --install grafana monitoring-tools/grafana/ && wait;
helm upgrade --atomic --install prometheus monitoring-tools/prometheus/ && wait;
helm upgrade --atomic --install alertmanager monitoring-tools/alertmanager/

