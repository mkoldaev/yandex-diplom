stages:
  - get-version
  - build
  - test
  - nexus
  - deploy

variables:
  IMAGE_TAG: "0.0"

include:
  - remote: 'https://gitlab.com/gitlab-org/gitlab/-/raw/2851f4d5/lib/gitlab/ci/templates/Jobs/SAST.latest.gitlab-ci.yml'

versionus:
  stage: get-version
  image:
    name: alpine/helm:3.5.3
  script: 
    - helm repo add koldaev-helm "${NEXUS_MOMO_REPO_URL}" --username ${NEXUS_REPO_USER} --password ${NEXUS_REPO_PASS}
    - helm repo update
    - export VERSION=$(helm search repo frontend | tail -n +2 | awk '{print $2}')
    - export NEXTVERSION=$(echo ${VERSION} | awk -F. -v OFS=. '{$NF += 1 ; print}')
    - echo $NEXTVERSION
    - echo "IMAGE_TAG=${NEXTVERSION}" >> build.env
  artifacts:
    reports:
      dotenv: build.env

build-frontend:
  stage: build
  image:
    name: gcr.io/kaniko-project/executor:v1.9.0-debug
    entrypoint: [""]
  script:
    - /kaniko/executor
      --context "${CI_PROJECT_DIR}/frontend"
      --dockerfile "${CI_PROJECT_DIR}/frontend/Dockerfile"
      --destination "${CI_REGISTRY_IMAGE}/momo-frontend:${IMAGE_TAG}"
      --cache=true
      --cache-ttl=6h

sonarqube-frontend-sast:
  stage: test
  rules:
    - if: '$CI_COMMIT_BRANCH == "master"'
  allow_failure: true
  variables:
    SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"    
    GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task
  image:
    name: sonarsource/sonar-scanner-cli:latest
    entrypoint: [""]
  cache:
    key: "${CI_JOB_NAME}_front"
    paths:
      - .sonar/cache   
  script:
    - cd frontend
    - sonar-scanner -Dsonar.host.url=${SONARQUBE_URL} -Dsonar.sources="src" -Dsonar.login=${SONAR_LOGIN_FRONT} -Dsonar.projectKey=${SONAR_PROJECT_KEY_FRONT} -Dsonar.projectName="${SONAR_PROJECTNAME_FRONT}" -Dsonar.qualitygate.wait=true
  needs:
    - build-frontend

package-nexus:
  stage: nexus
  image:
    name: alpine/helm:3.5.3
  script: 
    - echo "${IMAGE_TAG}"
    - apk add curl
    - cat ./helm/frontend/Chart.yaml
    - 'sed -i "s/version:.*/version: \"${IMAGE_TAG}\"/g" ./helm/frontend/values.yaml'
    - 'sed -i "s/version:.*/version: \"${IMAGE_TAG}\"/g" ./helm/frontend/Chart.yaml'
    - cat ./helm/frontend/Chart.yaml
    - cd ./helm/frontend
    - helm package .
    - wait
    - curl --fail -v  -u "${NEXUS_REPO_USER}:${NEXUS_REPO_PASS}" "${NEXUS_MOMO_REPO_URL}/" --upload-file ./frontend-${IMAGE_TAG}.tgz
  needs:
    - versionus
    - build-frontend

deploy-frontend:
  stage: deploy
  image: alpine:3.18
  script:
    - apk update && apk add gettext openssh-client curl || true
    - curl ident.me
    - command -v ssh-agent >/dev/null
    - eval $(ssh-agent -s)
    - echo "${SSH_PRIVATE_KEY}" | tr -d '\r' | ssh-add -
    - mkdir -p ~/.ssh
    - chmod 700 ~/.ssh
    - echo "$SSH_KNOWN_HOSTS" >> ~/.ssh/known_hosts
    - chmod 644 ~/.ssh/known_hosts
    - ssh ${DEV_USER}@${DEV_HOST} "pwd && wait && helm repo update && helm upgrade --atomic --install frontend koldaev-helm/frontend || true && wait && helm list"
  environment:
    name: development
    url: http://${DEV_HOST} 
    auto_stop_in: 1h
  rules: # Специальное условие, требующее ручного нажатия для установки на виртуальную машину нового обновления
    - when: manual
  needs:
    - build-frontend
    - package-nexus