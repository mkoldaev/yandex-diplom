module "tf-yc-instance" {
  source = "./modules/tf-yc-instance"
  disk_size = var.disk_size
  platform_id = var.platform_id
  instance_zone = var.zone
  scheduling_policy = {}
}
module "tf-yc-network" {
  source = "./modules/tf-yc-network"
  network_name = "default"
  private_subnets = [{
    name           = "subnet-1"
    zone           = "ru-central1-a"
    v4_cidr_blocks = ["10.10.0.0/24"]
  },
  {
    name           = "subnet-2"
    zone           = "ru-central1-b"
    v4_cidr_blocks = ["10.11.0.0/24"]
  },
  {
    name           = "subnet-3"
    zone           = "ru-central1-c"
    v4_cidr_blocks = ["10.12.0.0/24"]
  }
  ]
}