resource "yandex_compute_instance" "vm-1" {
    name = "k8s1"
    zone = var.instance_zone
    resources {
        cores  = var.cores
        memory = var.memory
    }
    boot_disk {
        initialize_params {
            image_id = var.image_id
            size = var.disk_size
        }
    }
    network_interface {
        subnet_id = var.subnet_id
        nat       = true
    }
    metadata = {
        ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
}
resource "yandex_compute_instance" "vm-2" {
    name = "k8s2"
    zone = var.instance_zone
    resources {
        cores  = var.cores
        memory = var.memory2
    }
    boot_disk {
        initialize_params {
            image_id = var.image_id
            size = var.disk_size
        }
    }
    network_interface {
        subnet_id = var.subnet_id
        nat       = true
    }
    metadata = {
        ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
}
resource "yandex_compute_instance" "vm-3" {
    name = "k8s3"
    zone = var.instance_zone
    resources {
        cores  = var.cores
        memory = var.memory2
    }
    boot_disk {
        initialize_params {
            image_id = var.image_id
            size = var.disk_size
        }
    }
    network_interface {
        subnet_id = var.subnet_id
        nat       = true
    }
    metadata = {
        ssh-keys = "ubuntu:${file("~/.ssh/id_rsa.pub")}"
    }
}