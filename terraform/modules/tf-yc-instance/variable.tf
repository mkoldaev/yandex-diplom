variable "disk_size" {}
variable "instance_zone" {}
variable "platform_id" {}
variable "scheduling_policy" {}
variable "cores" { default = 2 }
variable "memory" { default = 4 }
variable "memory2" { default = 2 }
variable "image_id" { default = "fd89ovh4ticpo40dkbvd" }
variable "subnet_id" { default = "e2l4tnqg02pks2d11oov" }
