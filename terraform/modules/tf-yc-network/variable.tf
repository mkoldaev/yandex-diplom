variable "network_name" {}
variable "name" {
  type    = string
  default = "vpc"
}
variable "private_subnets" {}

variable "labels" {
  type        = map(string)
  description = "Labels to mark resources."
  default     = {}
}