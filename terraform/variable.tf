variable "cloud_id" {}
variable "folder_id" {}
variable "token" {}
variable "zone" {
  default = "ru-central1-b"
}
variable "disk_size" { default = 20 }
variable "platform_id" { default = "standard-v1" }
